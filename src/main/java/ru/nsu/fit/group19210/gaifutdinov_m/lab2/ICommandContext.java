package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.EmptyStackException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.ParameterAlreadyDefinedException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.ParameterNotDefinedException;

public interface ICommandContext {
    double peek() throws EmptyStackException;
    double pop() throws EmptyStackException;
    void push(double number);
    int stackSize();
    double getDefine(String arg) throws ParameterNotDefinedException;
    void setDefine(String arg, double x) throws ParameterAlreadyDefinedException;
}