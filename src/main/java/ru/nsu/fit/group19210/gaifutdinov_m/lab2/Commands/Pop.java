package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Pop extends Command {
    private static final Logger log = LogManager.getLogger(Pop.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Pop> starts executing.");

        context.pop();

        log.trace("<Pop> finishes executing.");
    }
}