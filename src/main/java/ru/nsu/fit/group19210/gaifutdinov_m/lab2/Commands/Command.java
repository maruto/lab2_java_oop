package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CalculatorException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

abstract public class Command {
    abstract public void execute(ICommandContext context, String[] arguments) throws CalculatorException;
}