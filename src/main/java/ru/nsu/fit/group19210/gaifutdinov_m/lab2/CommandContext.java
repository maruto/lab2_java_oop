package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.ParameterAlreadyDefinedException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.ParameterNotDefinedException;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class CommandContext implements ICommandContext {
    private static final Logger log = LogManager.getLogger(CommandContext.class);

    private final Map<String, Double> defines;
    private final Stack<Double> stack;

    CommandContext( ) {
        log.trace("Trying to create <CommandContext>");

        defines = new HashMap<>();
        stack = new Stack<>();

        log.trace("<CommandContext> created successfully");
    }

    public double peek() {
        return stack.peek();
    }

    public double pop() {
        return stack.pop();
    }

    public void push(double number){
        stack.push(number);
    }

    public int stackSize(){
        return stack.size();
    }

    public double getDefine(String arg) throws ParameterNotDefinedException {
        Double value = defines.get(arg);
        if(value == null) {
            throw new ParameterNotDefinedException();
        }else {
            return value;
        }
    }

    public void setDefine(String arg, double x) throws ParameterAlreadyDefinedException {
        Double value = defines.get(arg);
        if (value != null) {
            //если ключ есть то будет не null, при условии что мы всегда закидываем в мапу не null значение к ключу
            throw new ParameterAlreadyDefinedException();
        } else {
            defines.put(arg, x);
        }
    }
}
