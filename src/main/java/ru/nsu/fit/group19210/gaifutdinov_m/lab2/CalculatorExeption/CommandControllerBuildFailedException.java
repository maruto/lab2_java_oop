package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class CommandControllerBuildFailedException extends CalculatorException{
    public CommandControllerBuildFailedException(){
        super("Command controller build failed.");
    }
}
