package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class WrongSecondParameterException extends CalculatorException{
    public WrongSecondParameterException(String wrongParameter) {
        super("The second parameter must be a number: <" + wrongParameter +">");
    }
}