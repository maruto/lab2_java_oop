package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class ParameterAlreadyDefinedException extends CalculatorException{
    public ParameterAlreadyDefinedException() {
        super("This parameter is already defined.");
    }
}