package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Comment extends Command{
    private static final Logger log = LogManager.getLogger(Comment.class);

    public void execute(ICommandContext context, String[] arguments){
        log.trace("<Comment> executed successfully.");
    }
}