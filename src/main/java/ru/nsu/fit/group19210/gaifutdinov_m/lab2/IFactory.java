package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CalculatorException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands.Command;

public interface IFactory {
    Command getCommand(String command) throws CalculatorException;
}
