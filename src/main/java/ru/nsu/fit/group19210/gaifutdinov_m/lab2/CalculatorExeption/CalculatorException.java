package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class CalculatorException extends RuntimeException {
    public CalculatorException(String message){
        super(message);
    }
}

