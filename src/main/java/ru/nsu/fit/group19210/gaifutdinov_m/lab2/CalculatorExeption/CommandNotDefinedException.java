package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class CommandNotDefinedException extends CalculatorException{
    public CommandNotDefinedException(String command) {
        super("Command <"+ command + ">" + " not defined");
    }
}
