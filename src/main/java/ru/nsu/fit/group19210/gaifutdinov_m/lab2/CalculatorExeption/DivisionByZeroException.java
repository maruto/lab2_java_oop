package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class DivisionByZeroException extends CalculatorException {
    public DivisionByZeroException() {
        super("Division by zero");
    }
}
