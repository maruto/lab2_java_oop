package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class FactoryBuildFailException extends CalculatorException {
    public FactoryBuildFailException() {
        super("Factory create failed.");
    }
}
