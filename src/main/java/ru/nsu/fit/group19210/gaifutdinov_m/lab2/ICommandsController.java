package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CalculatorException;

public interface ICommandsController {
    void control(ICommandContext context, String inLine) throws CalculatorException;
}
