package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class EmptyStackException extends CalculatorException{
    public EmptyStackException() {
        super("Command cannot be executed. The stack is empty");
    }
}