package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class InvalidNumberOfArgumentsException extends CalculatorException {
    public InvalidNumberOfArgumentsException(String command) {
        super("Invalid number of arguments in " + command);
    }
}