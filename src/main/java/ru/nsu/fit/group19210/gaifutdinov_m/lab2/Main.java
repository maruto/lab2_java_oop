package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    static final Logger rootLogger = LogManager.getRootLogger();

    public static void main(String[] args) {
        rootLogger.trace("Program start working.");

        InputStream inSteam = null;
        try {
            if (args.length == 1) {
                inSteam = new FileInputStream(args[0]);
            } else
                inSteam = System.in;
        } catch (FileNotFoundException e) {
            rootLogger.fatal(e.getMessage());
            System.err.println("Fatal error: " + e.getMessage());
        }

        try(CalculatorExecutor calculator = new CalculatorExecutor(inSteam)){
            calculator.run();
        } catch (RuntimeException | IOException e) {
            rootLogger.fatal(e.getMessage());
            System.err.println("Fatal error.");
        }
        rootLogger.trace("Program finish working.");
    }
}
