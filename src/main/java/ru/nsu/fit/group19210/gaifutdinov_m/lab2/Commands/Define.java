package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Define extends Command{
    private static final Logger log = LogManager.getLogger(Define.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Define> starts executing.");

        try {
            double x = Double.parseDouble(arguments[1]);
            context.setDefine(arguments[0], x);
        } catch (NumberFormatException nfe) {
            log.error("<Define> throws <WrongSecondParameterException>");
            throw new WrongSecondParameterException(arguments[1]);
        } catch (ParameterAlreadyDefinedException e) {
            log.error("<Define> throws <ParameterAlreadyDefinedException>");
            throw e;
        }

        log.trace("<Define> finishes executing.");
    }
}
