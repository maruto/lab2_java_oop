package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CalculatorException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Add extends Command{
    private static final Logger log = LogManager.getLogger(Add.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Add> starts executing.");

        double x = context.pop();
        double y = context.pop();
        context.push(x + y);

        log.trace("<Add> finishes executing.");
    }
}
