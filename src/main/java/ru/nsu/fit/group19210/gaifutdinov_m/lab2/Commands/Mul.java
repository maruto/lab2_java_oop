package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Mul extends Command{
    private static final Logger log = LogManager.getLogger(Mul.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Mul> start executing.");

        double x = context.pop();
        double y = context.pop();
        context.push(x * y);

        log.trace("<Mul> finishes executing.");
    }
}