package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class GetCommandException extends CalculatorException{
    public GetCommandException() {
        super("Failed to create a command.");
    }
}
