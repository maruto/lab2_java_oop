package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;


public class Print extends Command {
    private static final Logger log = LogManager.getLogger(Print.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Print> starts executing.");

        System.out.println(context.peek());

        log.trace("<Print> finishes executing.");
    }
}