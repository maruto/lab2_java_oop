package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class NotEnoughNumbersOnStackException extends CalculatorException {
    public NotEnoughNumbersOnStackException(String command) {
        super("Not enough numbers on the stack to perform " + command);
    }
}
