package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CalculatorException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.NegativeRootException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

import static java.lang.Math.sqrt;

public class Sqrt extends Command {
    private static final Logger log = LogManager.getLogger(Sqrt.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Sqrt> starts executing.");

        if(context.peek() < 0){
            log.error("<Sqrt> throws <NegativeRootException>");
            throw new NegativeRootException();
        } else
            context.push(sqrt(context.pop()));

        log.trace("<Sqrt> finished executing.");
    }
}