package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands.Command;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

public class Factory implements IFactory{
    private static final Logger log = LogManager.getLogger(Factory.class);

    private final Map<String, Class<? extends Command>> commandsMap;


    public Factory(PropertyResourceBundle bundle) throws FactoryBuildFailException {
        log.trace("Trying to create <Factory>");

        commandsMap = new HashMap<>();
        var propKeys = bundle.getKeys();
        while (propKeys.hasMoreElements()) {
            String key = propKeys.nextElement();
            String currentParameters =  bundle.getString(key);

            String[] className =  currentParameters.split(" ");
            Class<?>  currentCommandClass = null;
            try {
                currentCommandClass = Class.forName(className[0]);
            }
            catch (ClassNotFoundException e){
                log.fatal("Could not find class for command <{}>", key);
                log.fatal("Fatal error. Program stop working.");
                throw new FactoryBuildFailException();
            }

            if(currentCommandClass.getSuperclass() == Command.class){
                commandsMap.put(key, (Class <? extends Command>)currentCommandClass);
            } else {
                log.fatal("<{}> does not implement the functionality of the abstract <Command>", currentCommandClass.getSimpleName());
                log.fatal("Fatal error. Program stop working.");
                throw new FactoryBuildFailException();
            }
        }

        log.trace("<Factory> created successfully");
    }

    public Command getCommand(String command) throws CalculatorException {
        log.trace("<Factory.getCommand()> start working");

        log.debug("Trying to find command \"" + command + "\".");
        var currentCommandClass = commandsMap.get(command);
        if(currentCommandClass == null){
            log.error("Command \"{}\" not defined in <commands.properties>", command);
            throw new CommandNotDefinedException(command);
        } else {
            log.debug("Command \"{}\" found successfully", command);
        }
        Command currCmd = null;
        try {
            currCmd = currentCommandClass.getDeclaredConstructor().newInstance();
        }
        catch (ReflectiveOperationException e) {
            log.fatal("Fatal error: " + e.getMessage());
            log.fatal("Fatal error in Factory.getCommand() method. Program stop working.");
            throw new GetCommandException();
        }
        log.trace("<Factory.getCommand()> finish working");
        return currCmd;
    }
}
