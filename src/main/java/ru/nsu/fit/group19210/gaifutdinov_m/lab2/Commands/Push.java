package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Push extends Command{
    private static final Logger log = LogManager.getLogger(Push.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Push> starts executing.");

        try{
            //проверяем есть ли дефайн, если нет то ловим исключение и добавляем агрумент на стэк,
            //иначе раздефайневаем и добавляем уже это значение на стэк
            double x = context.getDefine(arguments[0]);
            context.push(x);
        }
        catch (ParameterNotDefinedException err) {
            try{
                context.push(Double.parseDouble(arguments[0])); //строку в дабл и закидываем в стэк
            }
            catch (Exception e){
                log.error("<Push> throws <ParameterNotDefinedException>");
                throw err; //если пытаемся добавить на стек не задефайненыую строку
            }

        }

        log.trace("<Push> finishes executing.");
    }
}
