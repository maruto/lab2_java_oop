package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class ParameterNotDefinedException extends CalculatorException{
    public ParameterNotDefinedException() {
        super("This parameter not defined.");
    }
}
