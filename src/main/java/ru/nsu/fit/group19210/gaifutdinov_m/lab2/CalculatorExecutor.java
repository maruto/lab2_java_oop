package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CalculatorException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.GetCommandException;

import java.io.*;
import java.util.Arrays;
import java.util.PropertyResourceBundle;

public class CalculatorExecutor implements AutoCloseable {
    static final Logger log = LogManager.getLogger(CalculatorExecutor.class);

    private final ICommandsController controller;
    private final IFactory factory;
    private final ICommandContext context;

    private final LineNumberReader reader;

    public CalculatorExecutor(InputStream inStream) {
        log.trace("Trying to create <CalculatorExecutor>");

        String propFileName = "commands.properties";
        PropertyResourceBundle bundle;
        InputStream propInputFile = getClass().getResourceAsStream("/" + propFileName);
        try {
            bundle = new PropertyResourceBundle(propInputFile);
        }
        catch (NullPointerException | IOException e) {
            log.fatal("Unable to {} bundle", propFileName );
            log.fatal("Fatal error. Program stop working.");
            throw new RuntimeException("Unable to " + propFileName + " bundle");
        }

        controller = new CommandsController(bundle);
        factory = new Factory(bundle);
        context = new CommandContext();

        reader = new LineNumberReader(new BufferedReader(new InputStreamReader(inStream)));

        log.trace("<CalculatorExecutor> created successfully");
    }

    public void run() throws IOException {
        log.trace("<CalculatorExecutor.run()> start working.");

        String currentLine = reader.readLine();
        while(currentLine != null  && !currentLine.isBlank()){
            try {
                controller.control(context, currentLine);

                String[] currentArgs = currentLine.split(" ");

                log.debug("User entered \"" + currentLine + "\".");
                factory.getCommand(currentArgs[0]).execute(context, Arrays.copyOfRange(currentArgs, 1, currentArgs.length));
            }
            catch(GetCommandException e){
                System.err.println("Fatal error: " + e.getMessage());
            }
            catch (CalculatorException e) {
                System.err.println("Error: " + e.getMessage());
            }
            currentLine = reader.readLine();
        }
        log.trace("<CalculatorExecutor.run()> finish working.");
    }

    @Override
    public void close() throws IOException {
        log.trace("Trying to close <CalculatorExecutor>");
        reader.close();
        log.trace("<CalculatorExecutor> closed successfully");
    }
}