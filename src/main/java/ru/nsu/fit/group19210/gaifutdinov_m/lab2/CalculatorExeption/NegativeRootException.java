package ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption;

public class NegativeRootException extends CalculatorException {
    public NegativeRootException() {
        super("Negative root.");
    }
}
