package ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.ICommandContext;

public class Del extends Command{
    private static final Logger log = LogManager.getLogger(Del.class);

    public void execute(ICommandContext context, String[] arguments) throws CalculatorException {
        log.trace("<Del> starts executing.");

        double x = context.pop();
        if(x == 0){
            //если делим на ноль то закидываем ноль обратно на стэк и кидаем исключение
            context.push(x);
            log.error("<Del> throws <DivisionByZeroException>");
            throw new DivisionByZeroException();
        }
        double y = context.pop();
        context.push(y/x);

        log.trace("<Del> finishes executing.");
    }
}