package ru.nsu.fit.group19210.gaifutdinov_m.lab2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

public class CommandsController implements ICommandsController {
    private static final Logger log = LogManager.getLogger(CommandsController.class);

    private final Map<String, String[]> parametersMap;

    CommandsController(PropertyResourceBundle bundle) throws CommandControllerBuildFailedException {
        log.trace("Trying to create <CommandsController>");

        parametersMap = new HashMap<>();

        var propKeys = bundle.getKeys();
        while (propKeys.hasMoreElements()) {
            String key = propKeys.nextElement();
            String currentParameters =  bundle.getString(key);

            String[] currentArgs = currentParameters.split(" ");
            if(currentArgs.length != 3 ||
                    !(currentArgs[1].matches("[-+]?\\d+") &&  currentArgs[2].matches("[-+]?\\d+")) ||
                    (Integer.parseInt(currentArgs[1])<-1 || Integer.parseInt(currentArgs[2])<-1) ) {
                log.fatal("Incorrect data in <{}>.", key);
                log.fatal("Fatal error. Program stop working.");
                throw new CommandControllerBuildFailedException();
            }
            parametersMap.put(key,  Arrays.copyOfRange(currentArgs, 1, currentArgs.length));
        }

        log.trace("<CommandsController> created successfully");
    }

    public void control(ICommandContext context, String inLine) throws CalculatorException {
        log.trace("<CommandsController.control()> starts working.");

        String [] inputData = inLine.split(" ");
        String [] commandParameters = parametersMap.get(inputData[0]);
        if(commandParameters == null){
            log.error("Command {} not defined in <commands.properties>", inputData[0]);
            throw new CommandNotDefinedException(inputData[0]);
        }

        int numOfParInComm = Integer.parseInt(commandParameters[0]);
        int requiredNumOfElemInStack = Integer.parseInt(commandParameters[1]);

        if( (inputData.length - 1 != numOfParInComm) &&  (numOfParInComm != -1) ) {
            log.error("<InvalidNumberOfArgumentsException> in <{}>.",  inputData[0]);
            throw new InvalidNumberOfArgumentsException(inputData[0]);
        }

        if(requiredNumOfElemInStack > context.stackSize() &&  (requiredNumOfElemInStack != -1)) {
            log.error("<NotEnoughNumbersOnStackException> in <{}>.",  inputData[0]);
            throw new NotEnoughNumbersOnStackException(inputData[0]);
        }

        log.trace("<CommandsController.control()> finishes working.");
    }
}
