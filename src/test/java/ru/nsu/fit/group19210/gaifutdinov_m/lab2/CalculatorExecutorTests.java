package ru.nsu.fit.group19210.gaifutdinov_m.lab2;



import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;



public class CalculatorExecutorTests {
    @Test
    public void runTests() throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        System.setErr( new PrintStream(err));

        InputStream inputStreamOne = new ByteArrayInputStream("PUSH 5.5\nPUSH 5.5\n*\nPRINT\nPUSH 0\n\\\nPOP\nPOP\nPOP\nPUSH -1\nSQRT\nPUSH 4\n+\nPRINT\n".getBytes());
        CalculatorExecutor calculatorOne = new CalculatorExecutor(inputStreamOne);
        calculatorOne.run();
        assertEquals("30.25\r\n3.0\r\n", output.toString());
        assertEquals("Error: Command <\\> not defined\r\nError: Not enough numbers on the stack to perform POP\r\nError: Negative root.\r\n", err.toString());
    }
}
