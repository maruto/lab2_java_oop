package ru.nsu.fit.group19210.gaifutdinov_m.lab2;


import org.junit.jupiter.api.Test;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.CommandNotDefinedException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.FactoryBuildFailException;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands.Command;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.PropertyResourceBundle;

import static org.junit.jupiter.api.Assertions.*;


public class FactoryTests {
    @Test
    public void initTest() throws IOException {
        PropertyResourceBundle bundle_1, bundle_2, bundle_3;

        bundle_1 = new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_1=ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands.Push 1 -1".getBytes()));
        bundle_2 = new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_2=ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExecutor 0 0".getBytes()));
        bundle_3 = new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_3=path_1 0 0".getBytes()));

        assertThrows(FactoryBuildFailException.class, () -> new Factory(bundle_2));
        assertThrows(FactoryBuildFailException.class, () -> new Factory(bundle_3));
        assertNotEquals(null, new Factory(bundle_1));
    }
    @Test
    public void getCommandTesdt() throws IOException {
        PropertyResourceBundle bundle = new PropertyResourceBundle(getClass().getResourceAsStream("/commands.properties"));
        IFactory factory = new Factory(bundle);

        String command1 = "PUSH";
        String command2 = "PUSH123";
        assertEquals(Command.class, factory.getCommand(command1).getClass().getSuperclass());
        assertThrows(CommandNotDefinedException.class, () -> factory.getCommand(command2));
    }
}
