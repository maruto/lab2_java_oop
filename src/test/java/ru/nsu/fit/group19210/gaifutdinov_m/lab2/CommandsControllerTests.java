package ru.nsu.fit.group19210.gaifutdinov_m.lab2;



import org.junit.jupiter.api.Test;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.PropertyResourceBundle;

import static org.junit.jupiter.api.Assertions.*;



public class CommandsControllerTests {
    @Test
    public void initTest() throws IOException, CalculatorException {
        PropertyResourceBundle bundle_1, bundle_2, bundle_3;

        bundle_1 = new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_1=path_1 1 -1".getBytes()));
        bundle_2 = new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_2=path_2 1 -1 2".getBytes()));
        bundle_3 =  new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_3=path_3 4 -5".getBytes()));

        //Неверные параметры в файле properties
        assertThrows(RuntimeException.class, () -> new CommandsController(bundle_2) );
        assertThrows(RuntimeException.class, () -> new CommandsController(bundle_3) );

        assertNotEquals(null, new CommandsController(bundle_1)); //тут должен создаться экземпляр класса(не null)
    }
    @Test
    public void controlTest() throws IOException, CalculatorException {
        PropertyResourceBundle bundle;

        bundle = new PropertyResourceBundle(new ByteArrayInputStream("COMMAND_1=path_1 1 1\nCOMMAND_2=path_2 -1 -1".getBytes()));

        ICommandsController commandController = new CommandsController(bundle);

        ICommandContext context = new CommandContext();
        String line1 = "COMMAND_1 1";
        String line2 = "COMMAND 2";
        String line3 = "COMMAND_1";
        String line4 = "COMMAND_2";
        String line5 = "COMMAND_2 323 12 1g sd";

        assertThrows(NotEnoughNumbersOnStackException.class, () -> commandController.control(context, line1) );
        assertThrows(CommandNotDefinedException.class, () -> commandController.control(context, line2) );
        assertThrows(InvalidNumberOfArgumentsException.class, () -> commandController.control(context, line3) );

        context.push(1.5);
        assertDoesNotThrow(() -> commandController.control(context, line1));
        assertDoesNotThrow(() -> commandController.control(context, line4));
        assertDoesNotThrow(() -> commandController.control(context, line5));
    }
}
