package ru.nsu.fit.group19210.gaifutdinov_m.lab2;



import org.junit.jupiter.api.Test;

import ru.nsu.fit.group19210.gaifutdinov_m.lab2.CalculatorExeption.*;
import ru.nsu.fit.group19210.gaifutdinov_m.lab2.Commands.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;



public class CommandsTests {
    @Test
    public void addTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command add = new Add();

        String [] defaultArgs = {};
        context.push(5.5); context.push(5.5);   //Сложение положительны чисел
        context.push(1.0); context.push(-3.421);  //Сложение положительного с отрциательным
        context.push(-5.5); context.push(-3.3325);   //Сложение положительны чисел

        add.execute(context, defaultArgs);
        assertEquals(-5.5 - 3.3325, context.pop(), 10e-10);
        add.execute(context, defaultArgs);
        assertEquals(1.0 - 3.421, context.pop(), 10e-10);
        add.execute(context, defaultArgs);
        assertEquals(5.5 + 5.5, context.pop(), 10e-10);
    }
    @Test
    public void subTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command sub = new Sub();

        String [] defaultArgs = {};
        context.push(7.511); context.push(5.5);
        context.push(-7.511); context.push(5.5);
        context.push(-7.511); context.push(-5.5);
        context.push(7.511); context.push(-5.5);

        sub.execute(context, defaultArgs);
        assertEquals((-5.5) - (7.511), context.pop(), 10e-10);
        sub.execute(context, defaultArgs);
        assertEquals((-5.5)- (-7.511), context.pop(), 10e-10);
        sub.execute(context, defaultArgs);
        assertEquals((5.5) - (-7.511), context.pop(), 10e-10);
        sub.execute(context, defaultArgs);
        assertEquals((5.5)- (7.511), context.pop(), 10e-10);
    }
    @Test
    public void mulTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command mul = new Mul();

        String [] defaultArgs = {};
        context.push(7.511); context.push(5.5);
        context.push(0); context.push(5.5);
        context.push(-7.511); context.push(-5.5);
        context.push(7.511); context.push(-5.5);

        mul.execute(context, defaultArgs);
        assertEquals((7.511) * (-5.5), context.pop(), 10e-10);
        mul.execute(context, defaultArgs);
        assertEquals((-7.511) * (-5.5), context.pop(), 10e-10);
        mul.execute(context, defaultArgs);
        assertEquals((0) * (5.5), context.pop(), 10e-10);
        mul.execute(context, defaultArgs);
        assertEquals((7.511) * (5.5), context.pop(), 10e-10);
    }
    @Test
    public void delTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command del = new Del();

        String [] defaultArgs = {};
        context.push(7.511); context.push(5.5);
        context.push(5.5); context.push(0);
        context.push(-7.511); context.push(-5.5);
        context.push(7.511); context.push(-5.5);

        del.execute(context, defaultArgs);
        assertEquals((7.511) / (-5.5), context.pop(), 10e-10);
        del.execute(context, defaultArgs);
        assertEquals((-7.511) / (-5.5), context.pop(), 10e-10);

        //деление на ноль
        assertThrows(DivisionByZeroException.class, () -> new Del().execute(context, defaultArgs));
        context.pop(); context.pop();

        del.execute(context, defaultArgs);
        assertEquals((7.511) / (5.5), context.pop(), 10e-10);
    }
    @Test
    public void sqrtTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command sqrtCacl = new Sqrt();

        String [] defaultArgs = {};
        context.push(-9.0);
        context.push(2.0);
        context.push(0);
        context.push(49.0);

        sqrtCacl.execute(context, defaultArgs);
        assertEquals(Math.sqrt(49.0), context.pop(), 10e-10);
        sqrtCacl.execute(context, defaultArgs);
        assertEquals(Math.sqrt(0), context.pop(), 10e-10);
        sqrtCacl.execute(context, defaultArgs);
        assertEquals(Math.sqrt(2.0), context.pop(), 10e-10);
        assertThrows(NegativeRootException.class, () -> new Sqrt().execute(context, defaultArgs));//исключение - отрицательный корень
    }
    @Test
    public void commentTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command comment = new Comment();

        String [] defaultArgs = {};
        context.push(-9.0);

        comment.execute(context, defaultArgs);
        assertEquals(-9.0, context.pop(), 10e-10);
        //работа команды comment НЕ меняет контекст
    }
    @Test
    public void defineTest() throws CalculatorException{
        ICommandContext context = new CommandContext();
        Define define = new Define();


        String [] defaultArgs = new String[2];
        defaultArgs[0] = "a"; defaultArgs[1] = "1.0";
        define.execute(context, defaultArgs);
        assertEquals(1.0, context.getDefine("a"), 10e-10);

        defaultArgs[0] = "b"; defaultArgs[1] = "c"; //исключение - второй параметр долже быть числом
        assertThrows(WrongSecondParameterException.class, () -> new Define().execute(context, defaultArgs));

        defaultArgs[0] = "a"; defaultArgs[1] = "2";
        assertThrows(ParameterAlreadyDefinedException.class, () -> new Define().execute(context, defaultArgs));
    }
    @Test
    public void popTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command pop = new Pop();

        String [] defaultArgs = {};
        context.push(1.0);

        pop.execute(context, defaultArgs);
        assertThrows(java.util.EmptyStackException.class, context::pop); //исключение - пустой стэк
    }
    @Test
    public void pushTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command push = new Push();


        String [] defaultArgs = new String[1];
        defaultArgs[0] = "1.0";
        push.execute(context, defaultArgs);
        assertEquals(1.0, context.pop(), 10e-10);

        context.setDefine("a", 5.0);
        defaultArgs[0] = "a";
        push.execute(context, defaultArgs);
        assertEquals(5.0, context.pop(), 10e-10);

        defaultArgs[0] = "b";
        assertThrows(ParameterNotDefinedException.class, () -> new Push().execute(context, defaultArgs));
    }
    @Test
    public void printTest() throws CalculatorException {
        ICommandContext context = new CommandContext();
        Command printCalc = new Print();

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));


        String [] defaultArgs = {};
        //стандартный вывод
        context.push(5.0);
        printCalc.execute(context, defaultArgs);
        assertEquals("5.0\r\n", output.toString());

        System.setOut(null);
    }
}
